import './style.css';
import './app.css';

import logo from './assets/images/logo.jpg';
import {Greet,Greet1} from '../wailsjs/go/main/App';
import { main } from "../wailsjs/go/models";

// Setup the greet function
window.greet = function () {
    // Get name
    let name = nameElement!.value;

    // Check if the input is empty
    if (name === "") return;

    // Call App.Greet(name)
    try {
        Greet(name)
            .then((result) => {
                // Update result with data back from App.Greet()
                resultElement!.innerText = result;
            })
            .catch((err) => {
                console.error(err);
            });
    } catch (err) {
        console.error(err);
    }
};


window.greet1 = function () {
    // Get name
    let name1 = nameElement1!.value;

    // Check if the input is empty
    if (name1 === "") return;

    // Call App.Greet(name)
    try {

        let person = new main.Person();
        person.name = name1;
        person.age = 27;
        Greet1(person)
            .then((result1) => {
                // Update result with data back from App.Greet()
                resultElement1!.innerText = result1;
            })
            .catch((err) => {
                console.error(err);
            });
    } catch (err) {
        console.error(err);
    }
};


document.querySelector('#app')!.innerHTML = `
    <img id="logo" class="logo">
      <div class="result" id="result">Please enter your name below 👇</div>
      <div class="input-box" id="input">
        <input class="input" id="name" type="text" autocomplete="off" />
        <button class="btn" onclick="greet()">Greet</button>
      </div>
      <div class="result" id="result1">Please enter your name below 👇</div>
      <div class="input-box" id="input1">
        <input class="input" id="name1" type="text" autocomplete="off" />
        <button class="btn" onclick="greet1()">Greet1</button>
      </div>


    </div>
`;
(document.getElementById('logo') as HTMLImageElement).src = logo;

let nameElement = (document.getElementById("name") as HTMLInputElement);
nameElement.focus();
let resultElement = document.getElementById("result");

let nameElement1 = (document.getElementById("name1") as HTMLInputElement);
nameElement.focus();
let resultElement1 = document.getElementById("result1");


declare global {
    interface Window {
        greet: () => void;
    }

    interface Window {
        greet1: () => void;
    }

}
